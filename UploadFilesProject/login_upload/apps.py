from django.apps import AppConfig


class LoginUploadConfig(AppConfig):
    name = 'login_upload'
