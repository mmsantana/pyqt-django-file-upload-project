from django.db import models


class MyAbstract(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.CharField('Criado por', max_length=250)

    class Meta:
        abstract = True


class UploadFileModel(MyAbstract):
    attachment = models.FileField("Arquivo")
