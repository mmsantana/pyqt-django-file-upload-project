from django.conf.urls.static import static
from django.urls import path

from UploadFilesProject import settings
from .views import login_view, upload_files_view, login_api

app_name = 'login_upload'

urlpatterns = [
    path('', login_view, name='login'),
    path('upload_file/', upload_files_view, name='upload_file'),
    path('login_api/', login_api, name='login_api'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
