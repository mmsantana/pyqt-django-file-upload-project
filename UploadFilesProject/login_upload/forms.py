from django import forms

from .models import UploadFileModel


class UploadFileForm(forms.ModelForm):

    class Meta:
        model = UploadFileModel
        fields = ('attachment', )

        widgets = {
            'attachment': forms.ClearableFileInput(attrs={
                'class': 'form-control-file', 'placeholder': 'Arquivo',
                'accept': '.jpg, .jpeg, .png, .pdf', 'type': 'file', 'multiple': False
            }),
        }
