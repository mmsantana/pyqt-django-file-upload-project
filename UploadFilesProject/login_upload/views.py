import json

from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect

from UploadFilesProject.settings import API_TOKEN
from .models import UploadFileModel
from .forms import UploadFileForm


def login_api(request):
    username = request.GET.get('username')
    password = request.GET.get('password')
    token = request.GET.get('token')

    if not token:
        return HttpResponse(json.dumps([{'message': 'Token não informado!'}]), content_type='text/json')
    elif token != API_TOKEN:
        return HttpResponse(json.dumps([{'message': 'Token inválido!'}]), content_type='text/json')

    user = authenticate(username=username, password=password)

    if user:
        response = json.dumps([{'message': True}])
    else:
        response = json.dumps([{'message': False}])

    return HttpResponse(response, content_type='text/json')


def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            login(request, user)
            return redirect('upload_file/')
        else:
            context = {'login_error': 'Falha na autenticação! Tente novamente mais tarde.'}
            return render(request, 'login_page.html', context)
    else:
        if request.user.is_authenticated:
            return redirect('upload_file/')
        return render(request, 'login_page.html')


@login_required
def upload_files_view(request):
    files = UploadFileModel.objects.filter(created_by=request.user.username)
    if request.method == 'POST':
        form = UploadFileForm(request, request.FILES)
        if form.is_valid():
            new_form = form.save(commit=False)
            new_form.created_by = request.user.username
            new_form.save()
            return render(request, 'upload_file.html', {'form': form, 'files': files})
    else:
        form = UploadFileForm()
        return render(request, 'upload_file.html', {'form': form, 'files': files})
