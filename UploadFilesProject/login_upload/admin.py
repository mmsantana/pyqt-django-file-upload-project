from django.contrib import admin

from .models import UploadFileModel


class UploadFilesAdmin(admin.ModelAdmin):
    fields = ('attachment', )


admin.site.register(UploadFileModel, UploadFilesAdmin)
