import json
import os

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from login_upload.models import UploadFileModel


class TestUploadView(TestCase):

    def setUp(self) -> None:
        # Create users
        user = User.objects.create(username='test_user', is_active=True)
        user.set_password('temporary')
        user.save()
        user2 = User.objects.create(username='test_user2', is_active=True)
        user2.set_password('temporary')
        user2.save()
        self.upload_page = reverse('login_upload:upload_file')
        self.login_page = reverse('login_upload:login')
        self.login_api = reverse('login_upload:login_api')

    def tearDown(self) -> None:
        file = UploadFileModel.objects.last()
        if file:
            path_name = f'D:\\Programming Studies\\Python\\UploadFiles\\UploadFilesProject\\uploads\\{file.attachment}'
            if os.path.exists(path_name):
                os.remove(path_name)

    def test_login_page_get(self):
        self.client.login(username='test_user', password='temporary')

        response = self.client.get(self.login_page)

        self.assertEqual(response.status_code, 302)

    def test_login_api(self):

        response = self.client.get(
            f'{self.login_api}?username=test_user&password=temporary&token=48f9gs955gsd72rssfa31'
        )

        result = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(result[0].get('message'), True)

    def test_login_api_with_invalid_token(self):

        response = self.client.get(
            f'{self.login_api}?username=test_user&password=temporary&token=fdasdfasfasd72rssfa31'
        )

        result = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(result[0].get('message'), 'Token inválido!')

    def test_login_api_without_token(self):

        response = self.client.get(
            f'{self.login_api}?username=test_user&password=temporary'
        )

        result = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(result[0].get('message'), 'Token não informado!')

    def test_login_api_with_wrong_user(self):

        response = self.client.get(
            f'{self.login_api}?username=wrong_user&password=temporary&token=48f9gs955gsd72rssfa31'
        )

        result = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(result[0].get('message'), False)

    def test_upload_page_get(self):
        self.client.login(username='test_user', password='temporary')

        response = self.client.get(self.upload_page)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Realize o upload do seu arquivo!')

    def test_upload_page_post(self):
        self.client.login(username='test_user', password='temporary')

        with open('file_to_upload.jpg', "rb") as fp:
            response = self.client.post('/upload_file/', {'attachment': fp})

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Arquivos armazenados:')
        self.assertContains(response, 'file_to_upload.jpg')

    def test_upload_page_get_by_another_user(self):
        self.client.login(username='test_user', password='temporary')

        with open('file_to_upload.jpg', "rb") as fp:
            self.client.post('/upload_file/', {'attachment': fp})

        self.client.login(username='test_user2', password='temporary')
        response = self.client.get(self.upload_page)

        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'Arquivos armazenados:')
        self.assertNotContains(response, 'file_to_upload.jpg')
