import os

from django.contrib.auth.models import User
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver

from login_upload.models import UploadFileModel


class TestLoginUpload(StaticLiveServerTestCase):

    def setUp(self) -> None:
        self.browser = webdriver.Chrome('functional_tests/chromedriver.exe')
        self.browser.maximize_window()
        # Create user
        user = User.objects.create(username='test_user', is_active=True)
        user.set_password('temporary')
        user.save()

        self.browser.get(self.live_server_url)

    def tearDown(self) -> None:
        file = UploadFileModel.objects.last()
        if file:
            path_name = f'D:\\Programming Studies\\Python\\UploadFiles\\UploadFilesProject\\uploads\\{file.attachment}'
            if os.path.exists(path_name):
                os.remove(path_name)
        self.browser.close()

    def login_user(self):
        # The user fills its username and password to log into the application.
        self.browser.find_element_by_id('id_username').send_keys('test_user')
        self.browser.find_element_by_id('id_password').send_keys('temporary')
        self.browser.find_element_by_id('submit_login').click()

    def test_login_page(self):
        # The user finds the title and see if it corresponds to the login page.
        login_page_title = self.browser.find_element_by_id('login_title')
        self.assertEqual(login_page_title.text, 'Realize login para acessar a aplicação!')

        self.login_user()

        # The user sees if he got redirect to the upload page
        upload_page_title = self.browser.find_element_by_id('upload_title')
        self.assertEqual(upload_page_title.text, 'Realize o upload do seu arquivo!')

    def test_login_with_wrong_user(self):
        # The user finds the title and see if it corresponds to the login page.
        login_page_title = self.browser.find_element_by_id('login_title')
        self.assertEqual(login_page_title.text, 'Realize login para acessar a aplicação!')

        # The user fills its username and password to log into the application.
        self.browser.find_element_by_id('id_username').send_keys('test_user12')
        self.browser.find_element_by_id('id_password').send_keys('temporary2')
        self.browser.find_element_by_id('submit_login').click()

        # The user finds out that he wrote either the username or the password wrong.
        login_error = self.browser.find_element_by_id('login_error')
        self.assertEqual(login_error.text, 'Falha na autenticação! Tente novamente mais tarde.')

    def test_upload_page(self):
        self.login_user()

        # After login the user tries to upload a file.
        upload_file_title = self.browser.find_element_by_id('id_upload_file_title')
        self.assertEqual(upload_file_title.text, 'Arquivo:')
        self.browser.find_element_by_id('id_attachment').send_keys(
            'D:\\Programming Studies\\Python\\UploadFiles\\UploadFilesProject\\file_to_upload.jpg'
        )
        self.browser.find_element_by_id('save_file').click()

        # After uploading the file, the user sees if it was stored.
        uploaded_files_title = self.browser.find_element_by_id('id_uploaded_files_title')
        uploaded_files = self.browser.find_elements_by_class_name('uploaded_files')
        self.assertEqual(uploaded_files_title.text, 'Arquivos armazenados:')
        for file in uploaded_files:
            self.assertEqual('file_to_upload.jpg', file.text)
