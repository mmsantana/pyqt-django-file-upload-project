## Aplicação Django
** Para realizar as ações seguintes é necessário possuir qualquer versão do  
Python 3.x.

Primeiramente é necessário que todos os pacotes sejam instalados, 
para isso abra o prompt de comando na pasta em que o arquivo 
```requirements.txt``` se encontra.

Digite o seguinte comando para instalar todos os pacotes necessários:

```
pip install -r requirements.txt
```

Com os pacotes instalados, acesse pelo prompt a pasta que possui o arquivo 
manage.py e digite o comando para criar o banco de dados:

```
python manage.py makemigrations
python manage.py migrate
```

Digite o comando a seguir para criar um novo usuário:

```
python manage.py createsuperuser
```

Digite um nome, email e senha para acessar a aplicação.


Para iniciar o server:

```
python manage.py runserver
```

Acesse o endereço no seu navegador:

```
localhost:8000
```

Insira as credenciais cadastradas anteriormente pelo comando ```python manage.py createsuperuser```.

Insira o arquivo desejado através do botão apresentado na página.

### Testes

Os testes funcionais estão localizados na pasta ```functional_tests```.

Os testes unitários estão localizados na pasta ```login_upload/tests```.

Execute os comandos para gerar o relatório de test coverage:

```
coverage run --source='.' manage.py test
coverage report
```

O valor de test coverage apresentado foi de **96%**, já que os arquivos 
```manage.py```, ```apps.py``` e ```wsgi.py``` não são objetivamente testados.

## Aplicativo local (PyQt)

Para utilizar a aplicação é necessário ter realizado todos os passos 
presentes no tópico anterior.

Também certifique-se de que o comando foi executado:

```
python manage.py runserver
```

Na pasta ```PyQtProject```, execute o comando para iniciar a aplicação:

```
python app.py
```
