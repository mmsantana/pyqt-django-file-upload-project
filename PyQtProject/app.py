import sys
import json

import requests

from PyQt5.QtWidgets import (
    QApplication,
    QLineEdit,
    QMessageBox,
    QPushButton,
    QVBoxLayout,
    QWidget,
    QAction,
    QFileDialog
)
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject, Qt, QThread

from PyQtProject.request_login import querystring, headers


class LoginPage(QWidget):
    send_credentials = pyqtSignal(str, str)

    def __init__(self):
        super().__init__()
        self.title = "Login na aplicação"
        self.init_ui()

    def init_ui(self):

        self.setWindowTitle(self.title)

        # Create an username textbox
        self.username = QLineEdit(placeholderText="Usuário")
        self.password = QLineEdit(echoMode=QLineEdit.Password, placeholderText="Senha")
        self.button = QPushButton("Login")

        self.username.setFixedHeight(40)
        self.password.setFixedHeight(40)
        self.setFixedSize(380, 200)

        lay = QVBoxLayout(self)
        lay.addWidget(self.username)
        lay.addWidget(self.password)
        lay.addWidget(self.button, alignment=Qt.AlignLeft)

        # connect button to function on_click
        self.button.clicked.connect(self.on_click)

    @pyqtSlot()
    def on_click(self):
        username = self.username.text()
        password = self.password.text()

        self.send_credentials.emit(username, password)

    @pyqtSlot()
    def failed(self):
        QMessageBox.question(
            self, "Erro", "Usuário não autenticado!", QMessageBox.Ok, QMessageBox.Ok
        )
        self.username.clear()
        self.password.clear()


class UploadFilePage(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)

        save_file = QPushButton("Salvar Arquivo")
        save_file.clicked.connect(self.file_save)

        file_menu = QVBoxLayout(self)
        file_menu.addWidget(save_file)

    def file_save(self):
        name = QFileDialog.getSaveFileName(self, 'Salvar Arquivo')
        with open(name, 'w') as file:
            file.close()


class RequestsWorker(QObject):
    logged = pyqtSignal(bool)

    @pyqtSlot(str, str)
    def login_request(self, username, password):
        querystring.update({"username": username, "password": password})
        url = "http://localhost:8000/login_api/"
        response = requests.request("GET", url, headers=headers, params=querystring)
        result = json.loads(response.content)[0]["message"]
        self.logged.emit(result)


class Controller(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)

        thread = QThread(self)
        thread.start()

        self._requests_worker = RequestsWorker()
        self._requests_worker.logged.connect(self.on_logged)

        self._login_page = LoginPage()
        self._login_page.send_credentials.connect(self._requests_worker.login_request)
        self._login_page.show()

        self._upload_page = UploadFilePage()

    @pyqtSlot(bool)
    def on_logged(self, result):
        if result:
            self._login_page.close()
            self._upload_page.show()
        else:
            self._login_page.failed()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    controller = Controller()
    sys.exit(app.exec_())
